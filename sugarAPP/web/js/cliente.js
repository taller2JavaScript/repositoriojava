class Cliente {
static inicializar(){
		let elment_ins = document.querySelector("#btnAlta");
		elment_ins.setAttribute('onclick', "Cliente.alta()");
		/*let element_cons =document.querySelector("#btnConsultar");
		element_cons.setAttribute("onclick","Cliente.consultar()");*/
	}

	static alta(){
		const traer = async()=> {
			let cliente = {};
			cliente.nombre = document.querySelector("#nombre").value;
			cliente.apellido =document.querySelector("#apellido").value;
			cliente.dir =document.querySelector("#direccion").value;
			cliente.tel =document.querySelector("#telefono").value;
			let clienteStringJSON = JSON.stringify(cliente);
			let respuesta = await fetch ("./ClienteServer", {method: 'POST', body: clienteStringJSON});
			let datotexto = JSON.parse(await respuesta.text());
			document.querySelector("#panelResultados").innerHTML = datotexto;
		};

		traer()
			.catch(ex=> {
				document.querySelector("#panelMensajes").innerHTML= "error: " + ex.message + "¿?";
			});
	}
}
Cliente.inicializar();