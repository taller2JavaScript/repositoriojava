// Javascript: Inicializacion de Objeto PERSONA Instanciado con Llaves
Cliente = {};
// Creacion de un Metodo insertar() en el Objeto PERSONA
Cliente.alta = function(){
    // Instanciar el Objeto AJAX que existe en todos los Navegadores Web
    var xhr = new XMLHttpRequest();
    // Metodo INSERTAR, Accion PersonaServer
    xhr.open("POST","./ClienteServer");
    //xhr.setRequestHeader('enctype', 'multipart/form-data');
    xhr.setRequestHeader('Content-Type', 'multipart/form-data');
    // Metodo Respuesta que se ejecuta en y muestra al finalizar el AJAX.
    xhr.onreadystatechange = function(){
        if( xhr.readyState === 4 && xhr.status === 200){ // Caso de OK 
            document.querySelector('#panelResultados').innerHTML += xhr.responseText + '<br/>';
        } // Lista de codigos de error https://tools.ietf.org/html/rfc7231
    };
    // objeto para enviar los parametros del formulario
    var cli = {}; // new Object();
    cli.nombre = document.querySelector("#nombre").value;
	cli.apellido =document.querySelector("#apellido").value;
	cli.dir =document.querySelector("#direccion").value;
	cli.tel =document.querySelector("#telefono").value;
    // formato del mensaje en JSON
    var clienteStringJSON = JSON.stringify(cli);
    // Activa el Envio por Red del Ajax
    xhr.send(  clienteStringJSON );
};
// Creacion de un Metodo consultar() en el Objeto PERSONA
Cliente.consultar = function(){
    // Instanciar el Objeto AJAX que existe en todos los Navegadores Web
    var xhr = new XMLHttpRequest();
    // Metodo CONSULTAR, Accion PersonaServer
    xhr.open("GET","./ClienteServer");
    // Metodo Respuesta que se ejecuta en y muestra al finalizar el AJAX.
    xhr.onreadystatechange = function(){
        if( xhr.readyState === 4 && xhr.status === 200){ // Caso de OK 
            /*document.querySelector('#panelResultados').innerHTML += xhr.responseText + '<br/>';*/
            var clientes = JSON.parse( xhr.responseText );
            var templateCliente = document.querySelector("#templateCliES6").innerHTML;
            document.querySelector('#panelResultados').innerHTML = eval( templateCliente);
        } // Lista de codigos de error https://tools.ietf.org/html/rfc7231
    };
    // Activa el Envio por Red del Ajax
    xhr.send( );
};

// Inicializa en la pagina HTML los Eventos que ejecutan el Javascript
Cliente.inicializar = function(){
    // Agregar en los botones visuales HTML, las acciones Javascript
   let elment_ins = document.querySelector("#btnAlta");
   elment_ins.setAttribute('onclick', "Cliente.alta()");
   let element_cons =document.querySelector("#btnConsultar");
   element_cons.setAttribute("onclick","Cliente.consultar()");
};
// Se llama a la inicializacion de la pagina, 
// incorpora de forma NO Intrusiva el codigo javascript en el html
Cliente.inicializar();

