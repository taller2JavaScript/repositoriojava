package sugar;

/**
 *
 * @author Juan Pablo
 */
public class Clientes {
    private String idCli;
    private String nomCli;
    private String apeCli;
    private String direCli;
    private String telCli;  
   
  

    public String getIdCli() {
        return idCli;
    }

    public void setIdCli(String idCli) {
        this.idCli = idCli;
    }

    public String getNomCli() {
        return nomCli;
    }

    public void setNomCli(String nomCli) {
        this.nomCli = nomCli;
    }

    public String getApeCli() {
        return apeCli;
    }

    public void setApeCli(String apeCli) {
        this.apeCli = apeCli;
    }

    public String getDireCli() {
        return direCli;
    }

    public void setDireCli(String direCli) {
        this.direCli = direCli;
    }

    public String getTelCli() {
        return telCli;
    }

    public void setTelCli(String telCli) {
        this.telCli = telCli;
    }
}