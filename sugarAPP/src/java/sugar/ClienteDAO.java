package sugar;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author Juan Pablo
 */
public class ClienteDAO {
    
    private ClienteDAO() throws ClassNotFoundException, IOException, SQLException{
        
    }
    private static ClienteDAO INSTANCE = null;
    
    public static ClienteDAO getInstance() throws ClassNotFoundException, IOException, SQLException{
        if (INSTANCE == null) {
            INSTANCE = new ClienteDAO();
        }
        return INSTANCE;
    }
    
    private final static String SQL_CLIENTES_SELECT = "select * from clientes;";
    
    public ArrayList<Clientes> obtener() throws ClassNotFoundException, IOException, SQLException{
        ArrayList<Clientes> listaCli = new ArrayList();
        Connection cnx = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            cnx = Conectar.getInstance().getConnection();
            ps = cnx.prepareStatement(SQL_CLIENTES_SELECT);
            rs = ps.executeQuery();
            Clientes cl = null;
            while (rs.next()){
                try{
                    cl = new Clientes();
                    cl.setIdCli(rs.getString("idCli"));
                    cl.setNomCli(rs.getString("nCli"));
                    cl.setApeCli(rs.getString("aCli"));
                    cl.setDireCli(rs.getString("dirCli"));
                    cl.setTelCli(rs.getString("telCli"));
                    
                }catch (SQLException ex){
                }
                listaCli.add(cl);
            }
        } finally{
            try{
                rs.close();
            } finally {
                try {
                    ps.close();
                } finally{
                    cnx.close();
                }
            }
        }
        return listaCli;
    }
    
    private final static String SQL_CLIENTES_INSERT = 
            "INSERT INTO clientes(nCli, aCli, dirCli, telCli) VALUES (?,?,?,?);";
    
    public static void insertar(Clientes cl) throws ClassNotFoundException, IOException, SQLException{
        Connection cnx = null;
        PreparedStatement ps = null;
        try{
            cnx = Conectar.getInstance().getConnection();
            ps = cnx.prepareStatement(SQL_CLIENTES_INSERT);
            
            ps.setString(1, cl.getNomCli());
            ps.setString(2, cl.getApeCli());
            ps.setString(3, cl.getDireCli());
            ps.setString(4, cl.getTelCli());
            ps.execute();
        } finally{
            try{
                ps.close();
            } finally {
                cnx.close();
            }
        }
    }
    
    private final static String SQL_CLIENTES_UPDATE ="update clientes set nCli =?, aCli=?,"
            + " set dirCli =?, set telCli=? where idCli = ?;";
    
    public static void actualizar(Clientes cl)throws ClassNotFoundException, IOException, SQLException {
        Connection cnx = null;
        PreparedStatement ps = null;
        try{
            cnx = Conectar.getInstance().getConnection();
            ps = cnx.prepareStatement(SQL_CLIENTES_SELECT);
            ps.setString(1, cl.getNomCli());
            ps.setString(2, cl.getApeCli());
            ps.setString(3, cl.getDireCli());
            ps.setString(4, cl.getTelCli());
            ps.setInt(5, Integer.parseInt(cl.getIdCli()));
            ps.execute();
        }finally{
            try{
                ps.close();
            } finally {
                cnx.close();
            }
        }
    }
    
    private final static String SQL_CLIENTES_DELETE ="delete from clientes where idCli = ?;";
    
    public static void borrar(Clientes cl) throws ClassNotFoundException, IOException, SQLException{
        Connection cnx = null;
        PreparedStatement ps = null;
        try{
            cnx = Conectar.getInstance().getConnection();
            ps = cnx.prepareStatement(SQL_CLIENTES_SELECT);
            ps.setInt(1,Integer.parseInt(cl.getIdCli()));
            ps.execute();
        } finally {
            try {
                ps.close();
            } finally {
                cnx.close();
            }
        }
    }
    
    /////////////////////////
    // PROBAR CLIENTE DAO  //
    ////////////////////////
    
    public static void main (String[] args) {
        System.out.println("Testeando Cliente Dao");
        try{
            ArrayList<Clientes> listaCli = ClienteDAO.getInstance().obtener();
            for (Clientes cl : listaCli){
                System.out.println(cl);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        try{
            Clientes cl = new Clientes();
            cl.setNomCli("Seba");
            cl.setApeCli("Paragua");
            cl.setDireCli("Grillo");
            cl.setTelCli("55555");
            ClienteDAO.insertar(cl);
            
        }catch (Exception ex){
            ex.printStackTrace();
        }
        System.out.println("TESTEO OK");
    }
}
