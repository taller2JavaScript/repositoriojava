
package sugar;

import com.google.gson.Gson;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;


/**
 *
 * @author Juan Pablo
 */
@WebServlet(name = "ClienteServer", urlPatterns ={"/ClienteServer"})
public class ClienteServer extends HttpServlet {
    final static Gson CONVERTIR = new Gson();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        try{
            String texto = request.getReader().readLine();
            ArrayList<Clientes> listadoCli = ClienteDAO.getInstance().obtener();
            String resultado = CONVERTIR.toJson(listadoCli);
            out.println("" + resultado);
        }catch (ClassNotFoundException ex){
            out.println("Verificar: " + ex.getMessage() + " ClassNotFoundException");   
        }catch (SQLException ex){
            out.println("Verificar: " + ex.getMessage() + " SQLException");
        }catch (Exception ex){
            out.println("Verificar: " + ex.getMessage() + " --Exception");
        }finally{
            out.close();
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        try{
            String texto = request.getReader().readLine();
            Clientes cliparametro = CONVERTIR.fromJson(texto, Clientes.class);
            ClienteDAO.insertar(cliparametro);
            out.println(CONVERTIR.toJson("ok"));
        }catch (ClassNotFoundException ex){
            out.println("Verificar: " + ex.getMessage() + " ClassNotFoundException");   
        }catch (SQLException ex){
            out.println("Verificar: " + ex.getMessage() + " SQLException ACA TOY");
        }catch (Exception ex){
            out.println("Verificar: " + ex.getMessage() + " ++ Exception"); // aca da el error 
        }finally{
            out.close();
        }
    }
    
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        try{
            Clientes cliparametro = CONVERTIR.fromJson(request.getParameter("q"), Clientes.class);
            ClienteDAO.borrar(cliparametro);
            out.println(CONVERTIR.toJson("ok"));
        }catch (ClassNotFoundException ex){
            out.println("Verificar: " + ex.getMessage());   
        }catch (SQLException ex){
            out.println("Verificar: " + ex.getMessage());
        }catch (Exception ex){
            out.println("Verificar: " + ex.getMessage());
        }finally{
            out.close();
        }
    }
    
}
