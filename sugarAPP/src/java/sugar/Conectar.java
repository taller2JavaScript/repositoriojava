package sugar;

import java.io.IOException;
import java.sql.*;

/**
 *
 * @author Juan Pablo
 */
public class Conectar {
    
    private static Conectar INSTANCE = null;
    private static String BASE ="jdbc:mysql://localhost:3306/ejemplo2";
    private static String US = "root";
    private static String CLAVE = "";
    
    public static Conectar getInstance() throws ClassNotFoundException, IOException, SQLException{
        if (INSTANCE == null ){
            INSTANCE = new Conectar();
        }
        return INSTANCE;
    }
    
    private Conectar() throws ClassNotFoundException, IOException, SQLException{
        
    }
    
    public Connection getConnection() throws ClassNotFoundException, IOException, SQLException{
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection(BASE, US, CLAVE);
    }
}
