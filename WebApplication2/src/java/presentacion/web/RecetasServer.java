package presentacion.web;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet(name = "RecetasServer", urlPatterns = {"/RecetasServer"})
public class RecetasServer extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //response.getWriter().print( "\"Fideos Caseros y Flan con Dulce.\"" ); // Consulta
        response.getWriter().print( 
                "[ { \"plato\" : \"Fideos Caseros y Flan con Dulce.\", "
                        + "\"precio\": 150},{ \"plato\" : "
                        + "\"Vacio con Papas Fritas.\", "
                        + "\"precio\": 250} ]" ); // Consulta
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Inserrcion
    }
    
}
